# Examples

For Vivado version 2018.3


## examples/example1

16-bit operations: division. This operation takes time.
Slow division is inherently iterative so it tends to take longer.

https://electronics.stackexchange.com/questions/280673/why-does-hardware-division-take-much-longer-than-multiplication
There are somewhat faster slow division algorithms than the simple ones, using lookup tables.
The SRT algorithm produces two bits per cycle. An error in such a table was the cause of the infamous Pentium FDIV bug (ca. 1994). Then there are so-called fast division algorithms.

Division is slower than multiplication because there is no direct, parallel method for calculating it.
Either there is an iteration, or hardware is copied to implement the iteration as cascaded (or pipelined) blocks.

100MHz clock, directly from the input.
Would be nice that we can tune the clock speed.
I have seen that a 16-bit divider (when using the / operator) can not work on 100MHz,

(single slot, west border input/output)
- Project mode non PR:      project_no_pr
- Non Project mode non PR:  non_project_no_pr
- PR GoAhead:               goahead_pr
- PR Impress:               impress_pr

## examples/casestudy-aes

