# Using IMPRESS with and Vivado 2018.3

# git clone https://github.com/des-cei/impress.git
# git reset --hard 051e053

# Notes for the coarse tutorial
# Adjust static_coarse_and_medium.tcl VIO interface probes according to the module interface
# Create the folder: <path to IMPRESS>/examples/sources/coarse/design_time/static/synth_checkpoint

# In file project_info changed fpga_chip = xc7z020clg484-1
# created a div module, removed other modules, renamed interface, removed the reset, changed parition size and location

# In the Vivado TCL console:
set path_to_IMPRESS "/home/jeroen/Xilinx/Projects/2018.3/zedboard/thesis/examples/example1/impress_pr"
import_files -norecurse $path_to_IMPRESS/examples/sources/coarse/design_time/static/reconf_part.vhd
source $path_to_IMPRESS/examples/sources/coarse/design_time/static/static_coarse_and_medium.tcl


# To "Create HDL wrapper" do the following:
# In the sources window, expand the desing_1_wrapper and right click on the design_1.bd file for this option

# Question: Do we only have to run Synthesis or also the Implementation?
# Answer: Only synthesis is required.

# open the synthesized design and
file mkdir $path_to_IMPRESS/examples/sources/coarse/design_time/static/synth_checkpoint
# Note: project_info, default: sources = ${local}/../sources/coarse/design_time/static/synth_checkpoint.dcp
# and not in folder synth_checkpoint
write_checkpoint -force $path_to_IMPRESS/examples/sources/coarse/design_time/static/static_system.dcp
write_checkpoint -force $path_to_IMPRESS/examples/sources/coarse/design_time/static/synth_checkpoint.dcp


# To use IMPRESS open a new Vivado window and introduce the following commands
# Note: impress.tcl should be IMPRESS.tcl
set path_to_IMPRESS "/home/jeroen/Xilinx/Projects/2018.3/zedboard/thesis/examples/example1/impress_pr"
source $path_to_IMPRESS/design_time/reconfiguration_tool/IMPRESS.tcl
implement_reconfigurable_design $path_to_IMPRESS/examples/coarse_grain/project_info


# After running the command, we get these errors:
#ERROR -> module: static, type: ERROR: [Common 17-55] 'get_property' expects at least one object.
#Resolution: If [get_<value>] was used to populate the object, check to make sure this command returns at least one valid object.

#ERROR -> partition: group1, module: div, type: ERROR: [Common 17-55] 'get_property' expects at least one object.
#Resolution: If [get_<value>] was used to populate the object, check to make sure this command returns at least one valid object.

# We try again with org. source

