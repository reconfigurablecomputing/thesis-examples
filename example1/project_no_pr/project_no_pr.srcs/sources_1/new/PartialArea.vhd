library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VComponents.all;

entity PartialArea is port (
  clk : in std_logic;
	x0y0_s2p_w : in std_logic_vector(31 downto 0);
	x0y0_p2s_w : out std_logic_vector(15 downto 0));
end PartialArea;

architecture Behavioral of PartialArea is

-- architecture_declaration
signal a : signed(15 downto 0);
signal b : signed(15 downto 0);
signal o : signed(15 downto 0);

begin

  a <= signed(x0y0_s2p_w(31 downto 16));
  b <= signed(x0y0_s2p_w(15 downto 0));
  x0y0_p2s_w <= std_logic_vector(o);
     
  process(clk, a, b)
  begin
    if rising_edge(clk) then
      -- o <= a + b; -- Timing is met
      o <= a / b; -- Timing is not met         
    end if;  
  end process;
  
end architecture Behavioral;
