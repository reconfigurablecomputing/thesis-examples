set_property PACKAGE_PIN Y9 [get_ports clk]
set_property PACKAGE_PIN R18 [get_ports reset]; # "BTNR"
set_property PACKAGE_PIN T18 [get_ports btn];  # "BTNU"

set_property PACKAGE_PIN F22 [get_ports {inp[0]}]
set_property PACKAGE_PIN G22 [get_ports {inp[1]}]
set_property PACKAGE_PIN H22 [get_ports {inp[2]}]
set_property PACKAGE_PIN F21 [get_ports {inp[3]}]
set_property PACKAGE_PIN H19 [get_ports {inp[4]}]
set_property PACKAGE_PIN H18 [get_ports {inp[5]}]
set_property PACKAGE_PIN H17 [get_ports {inp[6]}]
set_property PACKAGE_PIN M15 [get_ports {inp[7]}]

set_property PACKAGE_PIN T22 [get_ports {outp[0]}]
set_property PACKAGE_PIN T21 [get_ports {outp[1]}]
set_property PACKAGE_PIN U22 [get_ports {outp[2]}]
set_property PACKAGE_PIN U21 [get_ports {outp[3]}]
set_property PACKAGE_PIN V22 [get_ports {outp[4]}]
set_property PACKAGE_PIN W22 [get_ports {outp[5]}]
set_property PACKAGE_PIN U19 [get_ports {outp[6]}]
set_property PACKAGE_PIN U14 [get_ports {outp[7]}]


set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports reset]
set_property IOSTANDARD LVCMOS33 [get_ports btn]

set_property IOSTANDARD LVCMOS33 [get_ports {inp[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {inp[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {outp[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {outp[7]}]

# 100MHz
create_clock -name clk -period 10 -waveform {2.5 5} [get_ports clk]

# 25Mhz
#create_clock -name clk -period 40 -waveform {10 20} [get_ports clk]
