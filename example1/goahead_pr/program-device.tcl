set File $::env(BITFILE)
open_hw -quiet
connect_hw_server -quiet
open_hw_target -quiet
current_hw_device [get_hw_devices xc7z020_1]
set_property PROGRAM.FILE $File [get_hw_devices xc7z020_1]
program_hw_devices [get_hw_devices xc7z020_1] -quiet
