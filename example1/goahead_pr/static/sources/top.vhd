library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity top is
	port (
		outp 	: out std_logic_vector (7 downto 0);
		inp	  : in std_logic_vector (7 downto 0);
		clk		: in std_logic;
		reset	: in std_logic;
		btn	  : in std_logic
	);
end top;

architecture structural of top is

component PartialArea is
	port (
		x0y0_s2p_w : in std_logic_vector(31 downto 0);
		x0y0_p2s_w : out std_logic_vector(15 downto 0)
	);
end component PartialArea;

attribute DONT_TOUCH : string;
attribute DONT_TOUCH of inst_PartialArea: label is "TRUE";

-- state machine
type state_type is (s0, s1, s2);
signal state : state_type;

-- intermediate signals
signal a : signed(15 downto 0);
signal b : signed(15 downto 0);

signal x0y0_s2p_w : std_logic_vector(31 downto 0);
signal x0y0_p2s_w : std_logic_vector(15 downto 0);

-- attribute_declaration
attribute s : string;
attribute keep : string;

-- attribute_assignment
attribute s of x0y0_s2p_w : signal is "true";
attribute s of x0y0_p2s_w : signal is "true";
attribute keep of x0y0_s2p_w : signal is "true";
attribute keep of x0y0_p2s_w : signal is "true";
begin

  inst_PartialArea : PartialArea
  port map (
	  x0y0_s2p_w => x0y0_s2p_w,
	  x0y0_p2s_w => x0y0_p2s_w
  );

  x0y0_s2p_w <= std_logic_vector(a) & std_logic_vector(b);


  -- output select
  process(clk, reset, btn)
  begin
    if rising_edge(clk) then
      if btn = '1' then
        outp <= x0y0_p2s_w(15 downto 8);
      else
        outp <= x0y0_p2s_w(7 downto 0);
      end if;
    end if;
  end process;


  -- state machine
  process(clk, reset)
  begin
    if reset = '1' then
      state <= s0;
    elsif rising_edge(clk) then
      case state is
        when s0 => state <= s1;
        when s1 => state <= s2;
        when s2 => state <= s0;
        when others => state <= s0;
      end case;
    end if;
  end process;


  -- provide input data
  process(state, inp)
  begin
    case state is
      when s0 =>
        a <= to_signed(4321, a'length);
        b <= to_signed(7, b'length);
      when s1 =>
        a <= to_signed(3145, a'length);
        b <= to_signed(3, b'length);
      when s2 =>
        a <= to_signed(2550, a'length);
        b <= resize(signed(inp), 16);
      when others =>
        a <= to_signed(1, a'length);
        b <= to_signed(1, b'length);
    end case;
  end process;


end structural;
