-- This file was automatically generated by GoAhead

-- Normally, there is no need for modifying this file.
-- However, there are comments with the keywords "architecture_declaration" 
-- and "architecture_body" that easily allow adding extra code to this file
-- Example for adding "internal_signal_a <= internal_signal_b" to the body of your_file.vhd:
-- cat your_file.vhd | sed 's/-- architecture_body/internal_signal_a := internal_signal_b;/g' | tr ":" "<" > out.vhd
-- Note that we use ":=" instead of "<=", which is corrected by tr (translate)

----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ConnectionPrimitiveWestInput is port (
	x0y0_s2p_w : out std_logic_vector(7 downto 0));

end ConnectionPrimitiveWestInput;

architecture Behavioral of ConnectionPrimitiveWestInput is

-- architecture_declaration

attribute s : string;
attribute keep : string;
attribute MARK_DEBUG : string;

-- attribute assignments

attribute MARK_DEBUG of x0y0_s2p_w : signal is "TRUE";

begin

-- architecture_body

-- instantiation of inst_x0y0_w_in_0
inst_x0y0_w_in_0 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(0),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_1
inst_x0y0_w_in_1 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(1),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_2
inst_x0y0_w_in_2 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(2),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_3
inst_x0y0_w_in_3 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(3),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_4
inst_x0y0_w_in_4 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(4),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_5
inst_x0y0_w_in_5 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(5),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_6
inst_x0y0_w_in_6 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(6),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_in_7
inst_x0y0_w_in_7 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_s2p_w(7),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '1',
	I4 => '0',
	I5 => '0'
);

end architecture Behavioral;
-- This file was automatically generated by GoAhead

-- Normally, there is no need for modifying this file.
-- However, there are comments with the keywords "architecture_declaration" 
-- and "architecture_body" that easily allow adding extra code to this file
-- Example for adding "internal_signal_a <= internal_signal_b" to the body of your_file.vhd:
-- cat your_file.vhd | sed 's/-- architecture_body/internal_signal_a := internal_signal_b;/g' | tr ":" "<" > out.vhd
-- Note that we use ":=" instead of "<=", which is corrected by tr (translate)

----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ConnectionPrimitiveWestOutput is port (
	x0y0_p2s_w : in std_logic_vector(3 downto 0));

end ConnectionPrimitiveWestOutput;

architecture Behavioral of ConnectionPrimitiveWestOutput is

-- architecture_declaration

attribute s : string;
attribute keep : string;
attribute MARK_DEBUG : string;

-- attribute assignments

attribute MARK_DEBUG of x0y0_p2s_w : signal is "TRUE";

signal x0y0_dummy_w : std_logic_vector(3 downto 0) := (others => '1');
attribute s of x0y0_dummy_w : signal is "true";
attribute keep of x0y0_dummy_w : signal is "true";

begin

-- architecture_body

-- instantiation of inst_x0y0_w_out_8
inst_x0y0_w_out_8 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_dummy_w(0),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => x0y0_p2s_w(0),
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_out_9
inst_x0y0_w_out_9 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_dummy_w(1),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => x0y0_p2s_w(1),
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_out_10
inst_x0y0_w_out_10 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_dummy_w(2),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => x0y0_p2s_w(2),
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_x0y0_w_out_11
inst_x0y0_w_out_11 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => x0y0_dummy_w(3),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => x0y0_p2s_w(3),
	I4 => '0',
	I5 => '0'
);

end architecture Behavioral;
