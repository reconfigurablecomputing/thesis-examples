library ieee;
use ieee.std_logic_1164.all;

entity top is
	port (
		fclk_clk0 : in std_logic
	);
end top;

architecture structural of top is

component ConnectionPrimitiveWestInput is
	port (
		x0y0_s2p_w : out std_logic_vector(31 downto 0)
	);
end component ConnectionPrimitiveWestInput;

component ConnectionPrimitiveWestOutput is
	port (
		x0y0_p2s_w : in std_logic_vector(15 downto 0)
	);
end component ConnectionPrimitiveWestOutput;

component add is
	port (
		clk : in std_logic;
		a : in std_logic_vector(15 downto 0);
		b : in std_logic_vector(15 downto 0);
		o : out std_logic_vector(15 downto 0)
	);
end component add;

attribute DONT_TOUCH : string;
attribute DONT_TOUCH of inst_ConnectionPrimitiveWestInput: label is "TRUE";
attribute DONT_TOUCH of inst_ConnectionPrimitiveWestOutput: label is "TRUE";
attribute DONT_TOUCH of inst_Module: label is "TRUE";

signal s2pm : std_logic_vector(31 downto 0) := (others => '0');
signal pm2s : std_logic_vector(15 downto 0) := (others => '0');

begin

inst_ConnectionPrimitiveWestInput : ConnectionPrimitiveWestInput
port map (
	x0y0_s2p_w => s2pm
);

inst_ConnectionPrimitiveWestOutput : ConnectionPrimitiveWestOutput
port map (
	x0y0_p2s_w => pm2s
);

inst_Module : add
port map (
	clk => fclk_clk0,
	a => s2pm(31 downto 16),
	b => s2pm(15 downto 0),
	o => pm2s
);

end structural;
