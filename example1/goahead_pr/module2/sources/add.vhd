library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add is
	port (
		clk : in std_logic;
		a   : in std_logic_vector(7 downto 0);
		b   : in std_logic_vector(7 downto 0);
		o   : out std_logic_vector(7 downto 0)
	);
end add;



architecture behavioural of add is
begin

  process(clk)
  begin
	  if rising_edge(clk) then
		  o <= std_logic_vector(signed(a) + signed(b););
	  end if;
  end process;

end behavioural;
