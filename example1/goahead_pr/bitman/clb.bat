@echo off
rem Script intended for running under Windows

set projectdir=M:\2018.3\zedboard\example1

bitman.exe -v -c %projectdir%\Merge\bitdiff\merged.bit > merged-clb.txt
bitman.exe -v -c %projectdir%\Merge\bitdiff\merged_v2.bit > merged2-clb.txt
bitman.exe -v -c %projectdir%\Merge\bitdiff\staticsystem_final_clock_removed.bit > staticsystem-final-clock-removed-clb.txt

