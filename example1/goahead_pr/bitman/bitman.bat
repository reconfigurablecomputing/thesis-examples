@echo off
rem Script intended for running under Windows

set projectdir=M:\2018.3\zedboard\example1
bitman.exe -x 34 50 38 99 %projectdir%\module1\module.bit -M 34 50 %projectdir%\module1\module1-partial.bit
bitman.exe -x 34 50 38 99 %projectdir%\module2\module.bit -M 34 50 %projectdir%\module2\module2-partial.bit


echo Cut a partial bittream from the merged static system...
bitman.exe -x 34 50 38 99 %projectdir%\Merge\bitdiff\staticsystem_final.bit -M 34 50 %projectdir%\Merge\bitdiff\staticsystem-module1-partial-v1.bit
bitman.exe -x 34 50 38 99 %projectdir%\Merge\bitdiff\staticsystem_final_clock_removed.bit -M 34 50 %projectdir%\Merge\bitdiff\staticsystem-module1-partial-v2.bit

rem Merge partial bitstream
echo Merge...
bitman.exe -m 34 50 38 99 %projectdir%\module1\module.bit %projectdir%\static\static.bit -F %projectdir%\Merge\bitdiff\merged.bit
bitman.exe -m 34 50 38 99 %projectdir%\module1\module.bit %projectdir%\Merge\bitdiff\staticsytem_4ff_with_clock.bit -F %projectdir%\Merge\bitdiff\merged_v2.bit

rem bitman.exe -c %projectdir%\Merge\bitdiff\merged.bit > merged-clb.txt
rem bitman.exe -c %projectdir%\Merge\bitdiff\merged_v2.bit > merged2-clb.txt
rem bitman.exe -c %projectdir%\Merge\bitdiff\staticsystem-module1-partial-v2.bit > staticsystem-final-clock-removed-clb.txt
echo Merge...Done
