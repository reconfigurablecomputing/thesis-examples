library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity div is
	port (
		clk : in std_logic;
		a   : in std_logic_vector(15 downto 0);
		b   : in std_logic_vector(15 downto 0);
		o   : out std_logic_vector(15 downto 0)
	);
end div;



architecture behavioural of div is
begin

  process(clk, a, b)
  begin
	  if rising_edge(clk) then
		  o <= std_logic_vector(signed(a) / signed(b));
	  end if;
  end process;

end behavioural;
