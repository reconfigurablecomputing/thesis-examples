#!/usr/bin/bash

export BITFILE=static/static.bit
echo Loading the static bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

read -p "Press Enter to load the first module"

export BITFILE=module1/module1-partial.bit
echo Loading the partial bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

read -p "Press Enter to replace the first module"

export BITFILE=module2/module2-partial.bit
echo Loading the partial bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet
