#!/bin/bash

# Connect the board the computer and run this script
# Is it required to have the Hardware Server active in the background?

export BITFILE=staticsystem/static.bit
echo Loading the static bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

#read -p "Press Enter to load the first module"

export BITFILE=modules/config2/module-SubBytes-config2/module-partial-SubBytes.bit
echo Loading the partial bitstream SubBytes
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

export BITFILE=modules/config2/module-ShiftRows-config2/module-partial-ShiftRows.bit
echo Loading the partial bitstream ShiftRows
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

export BITFILE=modules/config2/module-MixColumns-config2/module-partial-MixColumns-1.bit
echo Loading the partial bitstream MixColumns 1
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

export BITFILE=modules/config2/module-MixColumns-config2/module-partial-MixColumns-2.bit
echo Loading the partial bitstream MixColumns 2
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet
