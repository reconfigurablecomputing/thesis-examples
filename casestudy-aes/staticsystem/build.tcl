# Read sources
read_vhdl [glob sources/*.vhd]
read_vhdl [glob goahead/sources/*.vhd]
read_xdc [glob constraints/*]

# Generate IPs
source ip-clocking-wizard.tcl

# Synthesize design
synth_design -part xc7z020clg484-1 -top top -keep_equivalent_registers -flatten_hierarchy none
#synth_design -part xc7z020clg484-1 -top top
write_checkpoint -force checkpoints/synthesis.dcp

# Optimize synthesized design
opt_design -sweep
write_checkpoint -force checkpoints/optimize-design.dcp

# Placement constraints
source goahead/tcl/static-placement-constraints.tcl
write_checkpoint -force checkpoints/placement-constraints.dcp

# Interface constraints
source goahead/tcl/static-interface-constraints.tcl
write_checkpoint -force checkpoints/interface-constraints.dcp

# Place
place_design
write_checkpoint -force checkpoints/place-design.dcp

# Clock connections
if {[file exists goahead/tcl/static-connect-clockpins.tcl]==1} {
  source goahead/tcl/static-connect-clockpins.tcl
  write_checkpoint -force checkpoints/clock-pins.dcp
}

# Insert blocker
source goahead/tcl/static-blocker.tcl
write_checkpoint -force checkpoints/blocker.dcp

# Route design
route_design -nets [get_nets -hierarchical -filter {TYPE != "GROUND"}]
write_checkpoint -force checkpoints/route-with-blocker.dcp

# Remove blocker
route_design -unroute -physical_nets

# Reroute other physical nets
route_design -physical_nets
write_checkpoint -force checkpoints/route-without-blocker.dcp

# Generate bitstream for BitMan
set_property BITSTREAM.General.UnconstrainedPins {Allow} [current_design]
set_property BITSTREAM.GENERAL.CRC DISABLE [current_design]
write_bitstream -force static.bit

