# create a Clocking Wizard IP from script

# get the current dir
set path_to_script [file dirname [file normalize [info script]]]
set ip_name "clk_wiz_0"

# clear or delete existing dir
file mkdir $path_to_script/ip
file delete -force $path_to_script/ip/$ip_name

create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name $ip_name -dir $path_to_script/ip
set_property -dict [list CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {20.000} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {8.500} CONFIG.MMCM_CLKOUT0_DIVIDE_F {42.500} CONFIG.CLKOUT1_JITTER {193.154} CONFIG.CLKOUT1_PHASE_ERROR {109.126}] [get_ips $ip_name]
generate_target {instantiation_template} [get_files $path_to_script/ip/$ip_name/$ip_name.xci]

synth_ip [get_ips $ip_name] -force

