----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.09.2019 13:45:50
-- Design Name: 
-- Module Name: Multiplexer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Multiplexer is
Port ( 
    sel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    i0 : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    i1 : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    i2 : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    i3 : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    i4 : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    i5 : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    o : OUT STD_LOGIC_VECTOR(127 DOWNTO 0));
end Multiplexer;

architecture Behavioral of Multiplexer is

begin

  o <= i0 when (sel = "000") else
       i1 when (sel = "001") else
       i2 when (sel = "010") else
       i3 when (sel = "011") else
       i4 when (sel = "100") else
       i5 when (sel = "101") else
       (others => '0');

end Behavioral;
