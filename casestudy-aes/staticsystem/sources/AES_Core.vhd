----------------------------------------------------------------------------------
-- COPYRIGHT (c) 2018 ALL RIGHT RESERVED
--
-- COMPANY:					Ruhr-University Bochum, Secure Hardware Group
-- AUTHOR:					Pascal Sasdrich and Jan Richter-Brockmann
--
-- CREATE DATA:			    13/11/2014
-- LAST CHANGES:            19/10/2018
-- MODULE NAME:			    AES_Core
--
--	REVISION:				2.00 - Simple round-based implementation
--
-- LICENCE: 				Please look at licence.txt
-- USAGE INFORMATION:	    Please look at readme.txt. If licence.txt or readme.txt
--							are missing or	if you have questions regarding the code
--							please contact Tim G�neysu (tim.gueneysu@rub.de) and
--							Pascal Sasdrich (pascal.sasdrich@rub.de) and
--                          Jan Richter-Brockmann (jan.richter-brockmann@rub.de)
--
-- THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
-- KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
-- PARTICULAR PURPOSE.
----------------------------------------------------------------------------------



-- IMPORTS
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;



-- ENTITY
----------------------------------------------------------------------------------
ENTITY AES_Core IS
	PORT (CLK 			: IN  STD_LOGIC;
	      RESET         : IN  STD_LOGIC;
		  -- CONTROL PORTS --------------------------------	
          DATA_AVAIL    : IN  STD_LOGIC;
          DATA_READY    : OUT STD_LOGIC;
          DPR_SEL       : IN  STD_LOGIC_VECTOR(5 DOWNTO 0);
  
		  -- DATA PORTS -----------------------------------
          KEY 			: IN  STD_LOGIC_VECTOR (127 DOWNTO 0);
          DATA_IN 	    : IN  STD_LOGIC_VECTOR (127 DOWNTO 0);
          DATA_OUT 	    : OUT STD_LOGIC_VECTOR (127 DOWNTO 0));
END AES_Core;



-- ARCHITECTURE
----------------------------------------------------------------------------------
ARCHITECTURE Structural OF AES_Core IS



-- SIGNALS
----------------------------------------------------------------------------------
-- AES
SIGNAL ROUND_IN, ROUND_OUT, ROUND_KEY	    : STD_LOGIC_VECTOR ( 127 DOWNTO 0);
SIGNAL IBUFFER, OBUFFER						: STD_LOGIC_VECTOR ( 127 DOWNTO 0);	

-- STATE MACHINE
SIGNAL ROUND_COUNT							: STD_LOGIC_VECTOR (  3 DOWNTO 0);

SIGNAL RESET_IO, ENABLE_IN, ENABLE_OUT	    : STD_LOGIC;
SIGNAL AES_INIT, AES_ENABLE, AES_LAST	    : STD_LOGIC;
SIGNAL KEY_INIT, KEY_ENABLE				    : STD_LOGIC;
SIGNAL CNT_RESET, CNT_ENABLE				: STD_LOGIC;



-- ATTRIBUTES
ATTRIBUTE KEEP : STRING;
ATTRIBUTE KEEP OF ROUND_IN 	     : SIGNAL IS "TRUE";
ATTRIBUTE KEEP OF ROUND_OUT      : SIGNAL IS "TRUE";



-- STRUCTURAL
----------------------------------------------------------------------------------
BEGIN

	-- ENCRYPTION
	IBUFFER	       <= DATA_IN;
	DATA_OUT       <= OBUFFER;
	
	-- I/O REGISTER ---------------------------------------------------------------
	InputBuffer : ENTITY work.RegisterFDRE GENERIC MAP (SIZE => 128)
	PORT MAP (D	=> IBUFFER, Q => ROUND_IN, CLK => CLK, EN => ENABLE_IN, RST => RESET_IO);
	
	OutputBuffer : ENTITY work.RegisterFDRE GENERIC MAP (SIZE => 128)
	PORT MAP (D	=> ROUND_OUT, Q => OBUFFER, CLK => CLK, EN => ENABLE_OUT, RST => RESET_IO);

	-- ROUND COUNTER --------------------------------------------------------------
	RoundCounter : ENTITY work.AES_Counter GENERIC MAP (SIZE => 4)
	PORT MAP (CLK => CLK, EN => CNT_ENABLE, RST => CNT_RESET, Q => ROUND_COUNT);
	
	-- AES KEYSCHEDULE ---------------------------------------------------------
	Keyschedule : ENTITY work.AES_Keyschedule
	PORT MAP (
		CLK			=> CLK,
		INIT		=> KEY_INIT,
		ENABLE		=> KEY_ENABLE,
		ROUND		=> ROUND_COUNT,
		KEY			=> KEY,
		ROUND_KEY	=> ROUND_KEY
	);
	
	-- AES ROUND FUNCTION ------------------------------------------------------------
	AESRound : ENTITY work.AES_Round
	PORT MAP (
		CLK			=> CLK,
		INIT		=> AES_INIT,
		ENABLE		=> AES_ENABLE,
		RST         => RESET,
		LAST		=> AES_LAST,
		DPR_SEL     => DPR_SEL,
		ROUND_KEY	=> ROUND_KEY,
		ROUND_IN	=> ROUND_IN,
		ROUND_OUT	=> ROUND_OUT
	);
	
	
	-- FINITE STATE MACHINE ----------------------------------------------------------
	FSM : ENTITY work.AES_Controller
	PORT MAP (
		CLK			=> CLK,
		DATA_AVAIL	=> DATA_AVAIL,
		DATA_READY	=> DATA_READY,
		RESET_IO	=> RESET_IO,
		ENABLE_IN	=> ENABLE_IN,
		ENABLE_OUT	=> ENABLE_OUT,
		AES_INIT	=> AES_INIT,
		AES_ENABLE	=> AES_ENABLE,
		AES_LAST	=> AES_LAST,
		KEY_INIT	=> KEY_INIT,
		KEY_ENABLE	=> KEY_ENABLE,
		CNT_RESET	=> CNT_RESET,
		CNT_ENABLE	=> CNT_ENABLE,
		ROUND_COUNT	=> ROUND_COUNT
	);	

END Structural;

