----------------------------------------------------------------------------------
-- COPYRIGHT (c) 2014 ALL RIGHT RESERVED
--
-- COMPANY:					Ruhr-University Bochum, Secure Hardware Group
-- AUTHOR:					Pascal Sasdrich and Jan Richter-Brockmann
--
-- CREATE DATA:			    12/03/2014
-- LAST CHANGES:            19/10/2018
-- MODULE NAME:			    AES_KeySchedule
--
-- REVISION:			    1.10 - Adapted to match a round-based implementation 
--
-- LICENCE: 				Please look at licence.txt
-- USAGE INFORMATION:	    Please look at readme.txt. If licence.txt or readme.txt
--							are missing or	if you have questions regarding the code
--							please contact Tim G�neysu (tim.gueneysu@rub.de) and
--							Pascal Sasdrich (pascal.sasdrich@rub.de) and
--                          Jan Richter-Brockmann (jan.richter-brockmann@rub.de)
--
-- THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
-- KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
-- PARTICULAR PURPOSE.
----------------------------------------------------------------------------------



-- IMPORTS
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;



-- ENTITY
----------------------------------------------------------------------------------
ENTITY AES_KeySchedule IS
	PORT (CLK		  : IN  STD_LOGIC;
		  INIT		  : IN  STD_LOGIC;
		  ENABLE	  : IN  STD_LOGIC;
	      ROUND	      : IN  STD_LOGIC_VECTOR (3 DOWNTO 0);
		  KEY	  	  : IN  STD_LOGIC_VECTOR (127 DOWNTO 0);
		  ROUND_KEY   : OUT STD_LOGIC_VECTOR (127 DOWNTO 0));
END AES_KeySchedule;



-- ARCHITECTURE
----------------------------------------------------------------------------------
ARCHITECTURE Dataflow OF AES_KeySchedule IS



-- CONSTANTS
----------------------------------------------------------------------------------
CONSTANT RCONS : STD_LOGIC_VECTOR (79 DOWNTO 0) := X"01020408102040801B36";



-- SIGNALS
----------------------------------------------------------------------------------
SIGNAL STATE, STATE_BUFFERED, CURRENT_KEY, NEXT_KEY	: STD_LOGIC_VECTOR (127 DOWNTO 0);
SIGNAL SB_IN, SB_OUT 								: STD_LOGIC_VECTOR (31 DOWNTO 0);
SIGNAL RCON 										: STD_LOGIC_VECTOR (7 DOWNTO 0);


-- ATTRIBUTES
----------------------------------------------------------------------------------
ATTRIBUTE KEEP : STRING;
ATTRIBUTE KEEP OF STATE_BUFFERED : SIGNAL IS "TRUE";



-- DATAFLOW
----------------------------------------------------------------------------------
BEGIN

	-- INPUT ----------------------------------------------------------------------
	STATE	 	    <= KEY WHEN (INIT = '1') ELSE NEXT_KEY;		
	
	-- INSTANCES ------------------------------------------------------------------
	RegA : ENTITY work.RegisterFDE
	GENERIC MAP (SIZE => 128)
	PORT MAP (
		D 	 => STATE,
		Q    => STATE_BUFFERED,
		CLK  => CLK,
		EN	 => ENABLE
	);
	
	-- OUTPUT ---------------------------------------------------------------------
	ROUND_KEY 		<= STATE_BUFFERED;
	CURRENT_KEY 	<= STATE_BUFFERED;
	
	-- S-BOXES --------------------------------------------------------------------
	Substitution :
	FOR I IN 0 TO 3 GENERATE
		SBoxes : ENTITY work.AES_SBox
		PORT MAP (
			S_IN   => SB_IN ((I*8)+7 DOWNTO (I*8)),
			S_OUT  => SB_OUT((I*8)+7 DOWNTO (I*8))
		);
	END GENERATE;
	
	-- KEY SCHEDULE ---------------------------------------------------------------	
	WITH ROUND SELECT
		RCON <= 	RCONS(79 DOWNTO 72) WHEN X"0",
					RCONS(71 DOWNTO 64) WHEN X"1",
					RCONS(63 DOWNTO 56) WHEN X"2",
					RCONS(55 DOWNTO 48) WHEN X"3",
					RCONS(47 DOWNTO 40) WHEN X"4",
					RCONS(39 DOWNTO 32) WHEN X"5",
					RCONS(31 DOWNTO 24) WHEN X"6",
					RCONS(23 DOWNTO 16) WHEN X"7",
					RCONS(15 DOWNTO  8) WHEN X"8",
					RCONS( 7 DOWNTO  0) WHEN OTHERS;
					
	SB_IN <= CURRENT_KEY(23 DOWNTO 0) & CURRENT_KEY(31 DOWNTO 24);	
	
	NEXT_KEY(127 DOWNTO 96) <= CURRENT_KEY(127 DOWNTO 96) XOR ((SB_OUT(31 DOWNTO 24) XOR RCON) & SB_OUT(23 DOWNTO 0));
	NEXT_KEY( 95 DOWNTO 64) <= CURRENT_KEY( 95 DOWNTO 64) XOR NEXT_KEY(127 DOWNTO 96);
	NEXT_KEY( 63 DOWNTO 32) <= CURRENT_KEY( 63 DOWNTO 32) XOR NEXT_KEY( 95 DOWNTO 64);
	NEXT_KEY( 31 DOWNTO  0) <= CURRENT_KEY( 31 DOWNTO  0) XOR NEXT_KEY( 63 DOWNTO 32);
	
END Dataflow;