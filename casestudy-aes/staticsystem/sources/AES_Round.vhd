----------------------------------------------------------------------------------
-- COPYRIGHT (c) 2014 ALL RIGHT RESERVED
--
-- COMPANY:					Ruhr-University Bochum, Secure Hardware Group
-- AUTHOR:					Pascal Sasdrich and Jan Richter-Brockmann
--
-- CREATE DATE:			    13/11/2014
-- LAST CHANGES:            19/10/2018
-- MODULE NAME:			    AES_Round
--
-- REVISION:				1.10 - Adapted to match a round-based implementation
--
-- LICENCE: 				Please look at licence.txt
-- USAGE INFORMATION:	    Please look at readme.txt. If licence.txt or readme.txt
--							are missing or	if you have questions regarding the code
--							please contact Tim G�neysu (tim.gueneysu@rub.de) and
--							Pascal Sasdrich (pascal.sasdrich@rub.de) and
--                          Jan Richter-Brockmann (jan.richter-brockmann@rub.de)
--
-- THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
-- KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
-- PARTICULAR PURPOSE.
----------------------------------------------------------------------------------



-- IMPORTS
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;



-- ENTITY
----------------------------------------------------------------------------------
ENTITY AES_Round IS
	PORT (   CLK		: IN  STD_LOGIC;
             -- CONTROL PORTS --------------------------------
             INIT		: IN  STD_LOGIC;
             ENABLE		: IN  STD_LOGIC;
             RST        : IN  STD_LOGIC;
             LAST		: IN  STD_LOGIC;
             -- DATA PORTS -----------------------------------
             ROUND_KEY	: IN  STD_LOGIC_VECTOR ( 127 DOWNTO 0);
             ROUND_IN	: IN  STD_LOGIC_VECTOR ( 127 DOWNTO 0);
			 ROUND_OUT	: OUT STD_LOGIC_VECTOR ( 127 DOWNTO 0);
			 -- OUTPUT PARTIAL AREA
             DPR_SEL: IN  STD_LOGIC_VECTOR(5 DOWNTO 0));
END AES_Round;



-- ARCHITECTURE
----------------------------------------------------------------------------------
ARCHITECTURE Structural OF AES_Round IS



-- SIGNALS
----------------------------------------------------------------------------------
SIGNAL STATE, NEXT_STATE, KEY_ADD, SB_IN, SB_OUT, SR_IN, SR_OUT, MC_IN, MC_OUT	: STD_LOGIC_VECTOR (127 DOWNTO 0);

signal x0y0_p2s_w : std_logic_vector(127 downto 0); --:= (others => '1');
signal x0y0_p2s_n : std_logic_vector(127 downto 0); --:= (others => '1');
signal x1y0_p2s_n : std_logic_vector(127 downto 0); --:= (others => '1');
signal x1y0_p2s_e : std_logic_vector(127 downto 0); --:= (others => '1');
signal x0y1_p2s_w : std_logic_vector(127 downto 0); --:= (others => '1');
signal x1y1_p2s_e : std_logic_vector(127 downto 0); --:= (others => '1');
signal  x0y0_s2p_w : std_logic_vector(127 downto 0); --:= (others => '1');
signal	x0y0_s2p_n : std_logic_vector(127 downto 0); --:= (others => '1');
signal	x1y0_s2p_n : std_logic_vector(127 downto 0); --:= (others => '1');
signal	x1y0_s2p_e : std_logic_vector(127 downto 0); --:= (others => '1');
signal	x0y1_s2p_w : std_logic_vector(127 downto 0); --:= (others => '1');
signal	x1y1_s2p_e : std_logic_vector(127 downto 0); --:= (others => '1');

-- attribute_declaration
  attribute s : string;
  attribute keep : string;

-- attribute_assignment
  attribute s of x0y0_p2s_w : signal is "true";
  attribute s of x0y0_s2p_w : signal is "true";
  attribute s of x0y0_p2s_n : signal is "true";
  attribute s of x0y0_s2p_n : signal is "true";
  attribute s of x1y0_p2s_n : signal is "true";
  attribute s of x1y0_s2p_n : signal is "true";
  attribute s of x1y0_p2s_e : signal is "true";
  attribute s of x1y0_s2p_e : signal is "true";
  attribute s of x0y1_p2s_w : signal is "true";
  attribute s of x0y1_s2p_w : signal is "true";
  attribute s of x1y1_p2s_e : signal is "true";
  attribute s of x1y1_s2p_e : signal is "true";
  attribute keep of x0y0_p2s_w : signal is "true";
  attribute keep of x0y0_s2p_w : signal is "true";
  attribute keep of x0y0_p2s_n : signal is "true";
  attribute keep of x0y0_s2p_n : signal is "true";
  attribute keep of x1y0_p2s_n : signal is "true";
  attribute keep of x1y0_s2p_n : signal is "true";
  attribute keep of x1y0_p2s_e : signal is "true";
  attribute keep of x1y0_s2p_e : signal is "true";
  attribute keep of x0y1_p2s_w : signal is "true";
  attribute keep of x0y1_s2p_w : signal is "true";
  attribute keep of x1y1_p2s_e : signal is "true";
  attribute keep of x1y1_s2p_e : signal is "true";


-- ATTRIBUTES
----------------------------------------------------------------------------------
--ATTRIBUTE KEEP : STRING;
ATTRIBUTE KEEP OF STATE 		: SIGNAL IS "TRUE";
ATTRIBUTE KEEP OF MC_IN			: SIGNAL IS "TRUE";
ATTRIBUTE KEEP OF MC_OUT		: SIGNAL IS "TRUE";
ATTRIBUTE KEEP OF NEXT_STATE	: SIGNAL IS "TRUE";



-- STRUCTURAL
-----------------------------------------------------------------------------------
BEGIN

	-- MULTIPLEXER (INPUT) --------------------------------------------------------
	STATE      <= ROUND_IN WHEN (INIT = '1') ELSE NEXT_STATE;

	-- KEY ADDITION (KA) ----------------------------------------------------------
	KEY_ADD	   <= STATE XOR ROUND_KEY;

	-- SUBBYTES REGISTER ----------------------------------------------------------
    AESSB_BUFFER : ENTITY work.RegisterFDRE GENERIC MAP (SIZE => 128)
    PORT MAP (D => KEY_ADD, Q => SB_IN, CLK => CLK, EN => ENABLE, RST => RST);

	-- SUB BYTES (SB) -------------------------------------------------------------
	--SB : ENTITY work.AES_SUBBYTES
	--PORT MAP (
	--	SB_IN		=> SB_IN,
	--	SB_OUT		=> SB_OUT
	--);

	--SR_IN <= SB_OUT;

	-- SHIFT ROWS (SR) ------------------------------------------------------------
    --SR : ENTITY work.AES_ShiftRows
    --PORT MAP (
    --    SR_IN        => SR_IN,
    --    SR_OUT        => SR_OUT
    --);


    --MC_IN <= SR_OUT;

	-- MIX COLUMNS (MC) -----------------------------------------------------------
	--MC : ENTITY work.AES_MixColumns
	--PORT MAP (
	--	MC_IN		=> MC_IN,
	--	MC_OUT	    => MC_OUT
	--);


	-- PARTIAL AREA ---------------------------------------------------------------

    -- We replace the subbytes, shiftrow and mixcolumn modules for the partial area
    -- Later on, we develop modules SubBytes, ShiftRows and MixColumn that is placed
    -- within the partial area during run-time to make the AES algorithm complete
    -- again

    x0y0_s2p_w <= SB_IN; -- required for config 2
    x0y0_s2p_n <= (others => '0'); --SB_IN;
    x1y0_s2p_n <= (others => '0'); --SB_IN;
    x1y0_s2p_e <= (others => '0'); --SB_IN;
    x0y1_s2p_w <= SB_IN; -- required for config 1
    x1y1_s2p_e <= (others => '0'); --SB_IN;

    inst_PartialArea : ENTITY work.PartialArea
    PORT MAP (
        x0y0_p2s_w => x0y0_p2s_w,
        x0y0_s2p_w => x0y0_s2p_w,
        x0y0_p2s_n => x0y0_p2s_n,
        x0y0_s2p_n => x0y0_s2p_n,
        x1y0_p2s_n => x1y0_p2s_n,
        x1y0_s2p_n => x1y0_s2p_n,
        x1y0_p2s_e => x1y0_p2s_e,
        x1y0_s2p_e => x1y0_s2p_e,
        x0y1_p2s_w => x0y1_p2s_w,
        x0y1_s2p_w => x0y1_s2p_w,
        x1y1_p2s_e => x1y1_p2s_e,
        x1y1_s2p_e => x1y1_s2p_e
    );

    -- The multiplexers select the output signals of the partial area
    -- We use two multiplexer to select two output signals from the partial area
    -- 1) Select the output of MixColumn module
    -- 2) Select the output of the ShiftRows module (which is input of the MixColumn module)

    inst_MultiplexerMixColumnsOut : ENTITY work.Multiplexer
    PORT MAP (
        sel => DPR_SEL(2 DOWNTO 0),
        i0 =>  x0y0_p2s_w,
        i1 =>  x0y0_p2s_n,
        i2 =>  x1y0_p2s_n,
        i3 =>  x1y0_p2s_e,
        i4 =>  x0y1_p2s_w,
        i5 =>  x1y1_p2s_e,
        o => MC_OUT
    );

    inst_MultiplexerShiftRowsOut : ENTITY work.Multiplexer
    PORT MAP (
        sel => DPR_SEL(5 DOWNTO 3),
        i0 =>  x0y0_p2s_w,
        i1 =>  x0y0_p2s_n,
        i2 =>  x1y0_p2s_n,
        i3 =>  x1y0_p2s_e,
        i4 =>  x0y1_p2s_w,
        i5 =>  x1y1_p2s_e,
        o => MC_IN
    );


	-- MULTIPLEXER (LAST ROUND) ---------------------------------------------------
	NEXT_STATE	<= MC_IN WHEN (LAST = '1') ELSE MC_OUT;

	-- OUTPUT ---------------------------------------------------------------------
	ROUND_OUT	<= KEY_ADD;

END Structural;

