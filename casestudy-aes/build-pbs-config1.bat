@ECHO OFF

REM Build the AES modules for configuration 1
REM Bitman binary was used under Windows
SET topdirectory=M:\2018.3\zedboard\thesis\examples\casestudy-aes

REM Create partial bitstream for SubBytes module
REM The module is developed on slot (0,1)
REM Build partial bitstream for slot (0,1)
CALL .\bitman\bitman.exe -x 34 0 37 49 %topdirectory%\modules\config1\module-SubBytes-config1\module.bit -M 34 0 %topdirectory%\modules\config1\module-SubBytes-config1\module-partial-SubBytes.bit

REM Create partial bitstream for ShiftRows module
REM The module is developed on slot (1,1)
REM Build partial bitstream for slot (1,1)
CALL .\bitman\bitman.exe -x 38 0 41 49 %topdirectory%\modules\config1\module-ShiftRows-config1\module.bit -M 38 0 %topdirectory%\modules\config1\module-ShiftRows-config1\module-partial-ShiftRows.bit

REM Create partial bitstream for MixColumns module
REM The module is developed on slot (1,0)
REM Build partial bitstream for slot (1,0)
CALL .\bitman\bitman.exe -x 38 50 41 99 %topdirectory%\modules\config1\module-MixColumns-config1\module.bit -M 38 50 %topdirectory%\modules\config1\module-MixColumns-config1\module-partial-MixColumns.bit

