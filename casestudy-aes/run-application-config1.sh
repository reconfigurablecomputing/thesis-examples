#!/bin/bash

export BITFILE=staticsystem/static.bit
echo Loading the static bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

#read -p "Press Enter to load the first module"

export BITFILE=modules/config1/module-SubBytes-config1/module-partial-SubBytes.bit
echo Loading the partial bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

export BITFILE=modules/config1/module-ShiftRows-config1/module-partial-ShiftRows.bit
echo Loading the partial bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet

export BITFILE=modules/config1/module-MixColumns-config1/module-partial-MixColumns.bit
echo Loading the partial bitstream
vivado -nolog -nojournal -notrace -mode batch -source program-device.tcl -quiet
