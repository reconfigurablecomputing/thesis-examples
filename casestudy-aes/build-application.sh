#!/bin/bash

# GoAhead stuff
#goahead static.goa
#MODULE_NAME=module1 goahead module.goa
#MODULE_NAME=module2 goahead module.goa

# Build designs
cd staticsystem
vivado -mode batch -nojournal -nolog -source build.tcl
#cd ../module1
#vivado -mode batch -nojournal -nolog -source build.tcl
#cd ../module2
#vivado -mode batch -nojournal -nolog -source build.tcl

# Extract partial bitstreams
#cd ..
#bitman -x 34 50 38 99 module1/module.bit -M 34 50 module1/module1-partial.bit
#bitman -x 34 50 38 99 module2/module.bit -M 34 50 module2/module2-partial.bit

# Currently, the bitstream containing the module is a full bitstream. We use BitMan to cut out the module area and generate a partial bitstream from this area.
# The slot size is 2 tiles width and 1 clock region high. The resource type is CLB_L+CLB_M. Thus, we can only relocate the module in CLB_L+CLB_M slots!
# The bitmap.exe takes the following parameters:
# bitman.exe  -x  left_bottom_X  left_bottom_Y  right_top_X  right_top_Y  full_bitstream  left_corner_X  left_corner_Y  partial_bitstream
