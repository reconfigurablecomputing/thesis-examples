#!/bin/bash


# Build designs
#cd staticsystem
#vivado -mode batch -nojournal -nolog -source build.tcl

cd modules/config2/module-MixColumns-config2
vivado -mode batch -nojournal -nolog -source build.tcl

cd ../module-ShiftRows-config2
vivado -mode batch -nojournal -nolog -source build.tcl

cd ../module-SubBytes-config2
vivado -mode batch -nojournal -nolog -source build.tcl

# Note: the bitman binary under linux gives an segmentation fault.
# I used the Windows binary instead
exit 0
