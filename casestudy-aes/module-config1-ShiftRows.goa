#-----------------------------------------------------------------------------------------
# Description: Module ShiftRow, configuration 1
# Footprint: 1x1-LsMLsM-W-NES
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# Global variables
#-----------------------------------------------------------------------------------------

Set Variable=ProjectDir Value=M:\2018.3\zedboard\thesis\examples\casestudy-aes;
Set Variable=ModuleName Value=module-ShiftRows-config1;
Set Variable=ZedBoard Value=%GOAHEAD_HOME%/Devices/xc7z020clg484.binFPGA;
Set Variable=GoAheadSources Value=%ProjectDir%/modules/config1/%ModuleName%/goahead/sources;
Set Variable=GoAheadTcl Value=%ProjectDir%/modules/config1/%ModuleName%/goahead/tcl;
Set Variable=GoAheadTemplates Value=%ProjectDir%/modules/config1/%ModuleName%/goahead/templates;

#-----------------------------------------------------------------------------------------
# Load device
#-----------------------------------------------------------------------------------------

OpenBinFPGA FileName=%ZedBoard%;

#-----------------------------------------------------------------------------------------
# Floorplan the module area
#-----------------------------------------------------------------------------------------

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y49 LowerRightTile=INT_R_X41Y0;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ModuleArea;

#-----------------------------------------------------------------------------------------
# Generate interface constraints
#-----------------------------------------------------------------------------------------

# Slot (1,1); Border - West
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y27 LowerRightTile=INT_R_X41Y22;
StoreCurrentSelectionAs UserSelectionType=X0Y0_WestBorder;
PrintInterfaceConstraintsForSelection
  FileName=%GoAheadTcl%/module-interface-constraints.tcl
  Append=False
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveWestInput
  Border=West
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=In:2-4:s2p_w;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_w
  NumberOfPrimitives=128;

AnnotateSignalNamesToConnectionPrimitives
  InstantiationFilter=inst_x0y0_w.*
  InputMappingKind=internal
  OutputMappingKind=external
  SignalPrefix=x0y0
  InputSignalName=1
  OutputSignalName=s2p_w
  LookupTableInputPort=3;

# Slot (1,1); Border - North
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y49 LowerRightTile=INT_R_X41Y44;
StoreCurrentSelectionAs UserSelectionType=X0Y0_NorthBorder;
PrintInterfaceConstraintsForSelection
  FileName=%GoAheadTcl%/module-interface-constraints.tcl
  Append=True
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveNorthOutput
  Border=North
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=Out:2-6:p2s_n;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_n
  NumberOfPrimitives=128;

AnnotateSignalNamesToConnectionPrimitives
  InstantiationFilter=inst_x0y0_n.*
  InputMappingKind=external
  OutputMappingKind=internal
  SignalPrefix=x0y0
  InputSignalName=p2s_n
  OutputSignalName=dummy_n
  LookupTableInputPort=3;

# Slot (1,1); Border - East
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y27 LowerRightTile=INT_R_X41Y22;
StoreCurrentSelectionAs UserSelectionType=X0Y0_EastBorder;
PrintInterfaceConstraintsForSelection
  FileName=%GoAheadTcl%/module-interface-constraints.tcl
  Append=True
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveEastOutput
  Border=East
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=Out:2-4:p2s_e;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_e
  NumberOfPrimitives=128;

AnnotateSignalNamesToConnectionPrimitives
  InstantiationFilter=inst_x0y0_e.*
  InputMappingKind=external
  OutputMappingKind=internal
  SignalPrefix=x0y0
  InputSignalName=p2s_e
  OutputSignalName=dummy_e
  LookupTableInputPort=3;

#-----------------------------------------------------------------------------------------
# Generate connection primitives in VHDL format
#-----------------------------------------------------------------------------------------

# connection primitives that are connected to the input signals of the module
PrintVHDLWrapper
  InstantiationFilter=inst_x0y0_w.*
  EntityName=ConnectionPrimitiveWestInput
  FileName=%GoAheadSources%/ConnectionPrimitive.vhd
  Append=False;

PrintVHDLWrapperInstantiation
  InstantiationFilter=inst_x0y0_w.*
  EntityName=ConnectionPrimitiveWestInput
  FileName=%GoAheadTemplates%/ConnectionPrimitive_inst.vhd
  Append=False;

# connection primitives that are connected to the output signals of the module
PrintVHDLWrapper
  InstantiationFilter=inst_x0y0_n.*
  EntityName=ConnectionPrimitiveNorthOutput
  FileName=%GoAheadSources%/ConnectionPrimitive.vhd
  Append=True;

PrintVHDLWrapperInstantiation
  InstantiationFilter=inst_x0y0_n.*
  EntityName=ConnectionPrimitiveNorthOutput
  FileName=%GoAheadTemplates%/ConnectionPrimitive_inst.vhd
  Append=True;

# connection primitives that are connected to the output signals of the module
PrintVHDLWrapper
  InstantiationFilter=inst_x0y0_e.*
  EntityName=ConnectionPrimitiveEastOutput
  FileName=%GoAheadSources%/ConnectionPrimitive.vhd
  Append=True;

PrintVHDLWrapperInstantiation
  InstantiationFilter=inst_x0y0_e.*
  EntityName=ConnectionPrimitiveEastOutput
  FileName=%GoAheadTemplates%/ConnectionPrimitive_inst.vhd
  Append=True;

#-----------------------------------------------------------------------------------------
# Tunnels
#-----------------------------------------------------------------------------------------

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y27 LowerRightTile=INT_R_X37Y22;
StoreCurrentSelectionAs UserSelectionType=InputWestTunnel;
DoNotBlockDoubleEast;
DoNotBlockQuadEast;

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y54 LowerRightTile=INT_R_X41Y50;
StoreCurrentSelectionAs UserSelectionType=OutputNorthTunnel;
DoNotBlockDoubleNorth;
DoNotBlockSexNorth;

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X42Y27 LowerRightTile=INT_R_X51Y22;
StoreCurrentSelectionAs UserSelectionType=OutputEastTunnel;
DoNotBlockDoubleEast;
DoNotBlockQuadEast;

#-----------------------------------------------------------------------------------------
# Generate blocker macro
#-----------------------------------------------------------------------------------------

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y54 LowerRightTile=INT_R_X51Y0;
SelectUserSelection UserSelectionType=ModuleArea;
StoreCurrentSelectionAs UserSelectionType=BlockerArea;

BlockSelection
  PrintUnblockedPorts=False
  Prefix=blocker_net
  BlockWithEndPips=True
  NetlistContainerName=default_netlist_container;

SaveAsBlocker
  NetlistContainerNames=default_netlist_container
  FileName=%GoAheadTcl%/module-blocker.tcl;

#-----------------------------------------------------------------------------------------
# Generate placement constraints
#-----------------------------------------------------------------------------------------

# module area
ClearSelection;
SelectUserSelection UserSelectionType=ModuleArea;

PrintAreaConstraint
  InstanceName=inst_Module
  AddResources=True
  FileName=%GoAheadTcl%/module-placement-constraints.tcl
  Append=False
  CreateBackupFile=True;

PrintExcludePlacementProperty
  InstanceName=inst_Module
  FileName=%GoAheadTcl%/module-placement-constraints.tcl
  Append=True
  CreateBackupFile=True;

# connection primitives connected to the input interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X26Y34 LowerRightTile=INT_L_X28Y15;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveWestInput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveWestInput
  AddResources=True
  FileName=%GoAheadTcl%/module-placement-constraints.tcl
  Append=True
  CreateBackupFile=True;

# connection primitives connected to the output interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y144 LowerRightTile=INT_R_X41Y125;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveNorthOutput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveNorthOutput
  AddResources=True
  FileName=%GoAheadTcl%/module-placement-constraints.tcl
  Append=True
  CreateBackupFile=True;

# connection primitives connected to the output interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X60Y34 LowerRightTile=INT_L_X62Y15;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveEastOutput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveEastOutput
  AddResources=True
  FileName=%GoAheadTcl%/module-placement-constraints.tcl
  Append=True
  CreateBackupFile=True;

