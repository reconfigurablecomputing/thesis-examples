create_pblock pb_inst_Module; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_Module] -add {SLICE_X50Y0:SLICE_X55Y49}; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_Module] -add {RAMB18_X3Y0:RAMB18_X3Y18}; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_Module] -add {RAMB18_X3Y1:RAMB18_X3Y19}; # generated_by_GoAhead
add_cells_to_pblock [get_pblocks pb_inst_Module] [get_cells inst_Module]; # generated_by_GoAhead

set_property EXCLUDE_PLACEMENT true [get_pblocks pb_inst_Module]; # generated by GoAhead

create_pblock pb_inst_ConnectionPrimitiveWestInput; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_ConnectionPrimitiveWestInput] -add {SLICE_X26Y15:SLICE_X31Y34}; # generated_by_GoAhead
add_cells_to_pblock [get_pblocks pb_inst_ConnectionPrimitiveWestInput] [get_cells inst_ConnectionPrimitiveWestInput]; # generated_by_GoAhead

create_pblock pb_inst_ConnectionPrimitiveNorthOutput; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_ConnectionPrimitiveNorthOutput] -add {SLICE_X50Y125:SLICE_X55Y144}; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_ConnectionPrimitiveNorthOutput] -add {RAMB18_X3Y50:RAMB18_X3Y56}; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_ConnectionPrimitiveNorthOutput] -add {RAMB18_X3Y51:RAMB18_X3Y57}; # generated_by_GoAhead
add_cells_to_pblock [get_pblocks pb_inst_ConnectionPrimitiveNorthOutput] [get_cells inst_ConnectionPrimitiveNorthOutput]; # generated_by_GoAhead

create_pblock pb_inst_ConnectionPrimitiveEastOutput; # generated_by_GoAhead
resize_pblock [get_pblocks pb_inst_ConnectionPrimitiveEastOutput] -add {SLICE_X82Y15:SLICE_X87Y34}; # generated_by_GoAhead
add_cells_to_pblock [get_pblocks pb_inst_ConnectionPrimitiveEastOutput] [get_cells inst_ConnectionPrimitiveEastOutput]; # generated_by_GoAhead

