----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.09.2019 11:19:28
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top is
end top;

architecture Behavioral of top is
    -- signal declaration
    signal i_s2p : std_logic_vector(127 downto 0) := (OTHERS => '0');
    signal i_p2s : std_logic_vector(127 downto 0) := (OTHERS => '0');
begin
    
    inst_ConnectionPrimitiveWestInput : ENTITY work.ConnectionPrimitiveWestInput
	port map (
	    x0y0_s2p_w => i_s2p
	);
	
	inst_ConnectionPrimitiveNorthOutput : ENTITY work.ConnectionPrimitiveNorthOutput
    port map (
        x0y0_p2s_n => i_p2s
    );
	
	inst_ConnectionPrimitiveEastOutput : ENTITY work.ConnectionPrimitiveEastOutput
    port map (
        x0y0_p2s_e => i_p2s
    );
	
--    inst_ConnectionPrimitiveSouthOutput : ENTITY work.ConnectionPrimitiveSouthOutput
--    port map (
--        x0y0_p2s_s => i_p2s
--    );	
	
	inst_Module : ENTITY work.AES_ShiftRows
	port map (
	   SR_IN  => i_s2p,
	   SR_OUT => i_p2s
	);
	
end Behavioral;
