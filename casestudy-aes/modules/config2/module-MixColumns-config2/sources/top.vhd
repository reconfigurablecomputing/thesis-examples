----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.09.2019 09:21:26
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
--  Port ( );
end top;

architecture Behavioral of top is

    -- signal declaration
    signal i_s2p : std_logic_vector(127 downto 0) := (OTHERS => '0');
    signal i_p2s : std_logic_vector(127 downto 0) := (OTHERS => '0');

begin

    inst_ConnectionPrimitiveWestOutput : ENTITY work.ConnectionPrimitiveWestOutput
	port map (
	    x0y0_p2s_w => i_p2s
	);
	
	inst_ConnectionPrimitiveNorthInput : ENTITY work.ConnectionPrimitiveNorthInput
    port map (
        x1y0_s2p_n => i_s2p
    );
	
	
	inst_Module : ENTITY work.AES_MixColumns
	port map (
	   MC_IN  => i_s2p,
	   MC_OUT => i_p2s
	);

end Behavioral;
