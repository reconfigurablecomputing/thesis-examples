----------------------------------------------------------------------------------
-- COPYRIGHT (c) 2014 ALL RIGHT RESERVED
--
-- COMPANY:					Ruhr-University Bochum, Secure Hardware Group
-- AUTHOR:					Pascal Sasdrich
--
-- CREATE DATA:			    13/11/2014
-- MODULE NAME:			    AES_MixColumns
--
-- REVISION:				1.00 - File created
--
-- LICENCE: 				Please look at licence.txt
-- USAGE INFORMATION:	    Please look at readme.txt. If licence.txt or readme.txt
--							are missing or	if you have questions regarding the code
--							please contact Tim G�neysu (tim.gueneysu@rub.de) and
--							Pascal Sasdrich (pascal.sasdrich@rub.de)
--
-- THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
-- KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
-- PARTICULAR PURPOSE.
----------------------------------------------------------------------------------



-- IMPORTS
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;



-- ENTITY
----------------------------------------------------------------------------------
ENTITY AES_MixColumns IS
	PORT ( MC_IN  : IN  STD_LOGIC_VECTOR (127 DOWNTO 0);
		   MC_OUT : OUT STD_LOGIC_VECTOR (127 DOWNTO 0));
END AES_MixColumns;



-- ARCHITECTURE
----------------------------------------------------------------------------------
ARCHITECTURE Structural OF AES_MixColumns IS



-- STRUCTURAL
----------------------------------------------------------------------------------
BEGIN

	-- INSTANCES ------------------------------------------------------------------
	Column1 : ENTITY work.AES_MixSingleColumn
	PORT MAP (
		COLUMN => MC_IN (127 DOWNTO 96),
		RESULT => MC_OUT(127 DOWNTO 96)
	);
	
	Column2 : ENTITY work.AES_MixSingleColumn
	PORT MAP (
		COLUMN => MC_IN ( 95 DOWNTO 64),
		RESULT => MC_OUT( 95 DOWNTO 64)
	);
	
	Column3 : ENTITY work.AES_MixSingleColumn
	PORT MAP (
		COLUMN => MC_IN ( 63 DOWNTO 32),
		RESULT => MC_OUT( 63 DOWNTO 32)
	);
	
	Column4 : ENTITY work.AES_MixSingleColumn
	PORT MAP (
		COLUMN => MC_IN ( 31 DOWNTO  0),
		RESULT => MC_OUT( 31 DOWNTO  0)
	);
	-------------------------------------------------------------------------------
	
	-- DUMMY LOGIC
	dummy : ENTITY work.DummyLogic
	PORT MAP (
	   tmp_dummy_in(1919 downto 1792) => MC_IN,
       tmp_dummy_in(1791 downto 1664) => MC_IN,
       tmp_dummy_in(1663 downto 1536) => MC_IN,
       tmp_dummy_in(1535 downto 1408) => MC_IN,
       tmp_dummy_in(1407 downto 1280) => MC_IN,
       tmp_dummy_in(1279 downto 1152) => MC_IN,
       tmp_dummy_in(1151 downto 1024) => MC_IN,
       tmp_dummy_in(1023 downto 896) => MC_IN,
       tmp_dummy_in(895 downto 768) => MC_IN,
       tmp_dummy_in(767 downto 640) => MC_IN,
       tmp_dummy_in(639 downto 512) => MC_IN,
       tmp_dummy_in(511 downto 384) => MC_IN,
       tmp_dummy_in(383 downto 256) => MC_IN,
       tmp_dummy_in(255 downto 128) => MC_IN,
       tmp_dummy_in(127 downto 0) => MC_IN 
	);
END Structural;