#!/bin/bash


# Build designs
cd staticsystem
vivado -mode batch -nojournal -nolog -source build.tcl

cd ../modules/config1/module-MixColumns-config1
vivado -mode batch -nojournal -nolog -source build.tcl

cd ../module-ShiftRows-config1
vivado -mode batch -nojournal -nolog -source build.tcl

cd ../module-SubBytes-config1
vivado -mode batch -nojournal -nolog -source build.tcl

# Note: the bitman binary under linux gives an segmentation fault.
# I used the Windows binary instead
exit 0

# Create partial bitstream for SubBytes module
# The module is developed on slot (0,0)
# Build partial bitstream for slot (0,1)
bitman -x 34 50 37 99 modules/config1/module-SubBytes-config1/module.bit -M 34 0 module-SubBytes.bit

# Create partial bitstream for ShiftRows module
# The module is developed on slot (1,0)
# Build partial bitstream for slot (1,1)
bitman -x 38 50 41 99 modules/config1/module-ShiftRows-config1/module.bit -M 38 0 module-ShiftRows.bit

# Create partial bitstream for MixColumns module
# The module is developed on slot (1,0)
# Build partial bitstream for slot (1,0)
bitman -x 38 50 41 99 modules/config1/module-MixColumns-config1/module.bit -M 38 50 module-_MixColumns.bit

